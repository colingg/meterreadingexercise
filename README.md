# MeterReadingExercise

##### folder structures:
- Simple Solution.pdf - solution file
- test folder - contains the integration tests
- meter - contains the application codes
- ansible - contains the ansible scripts to create infrastructures

> please read the README.md inside each folder for more information.

##### to-do:
- put all the modules(application code/integration test suite/ansible playbook) in a CICD tool. 
> since all the modules are in a ready state, it should be pretty straight-forward to configure the steps in any CICD platform.