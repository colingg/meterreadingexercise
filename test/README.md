# Meter Readings Integration Test
this is a starting point for a integration test suite(cucumber based).

##### done:
- two simple scenarios - upload file to s3, then call the consumption API

#### to-do:
- currently the s3 bucket name, sqs queue url are all hard coded in codes, need to externailise them in a config file

#### run the test
- update aws credentials in `src/test/resources/aws.json`
- run `mvn test`