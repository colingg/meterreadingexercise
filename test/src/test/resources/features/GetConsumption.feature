Feature: Get Consumption

    Background:
        Given database/bucket is cleared

    Scenario: one input file
        Given input file <input-1.txt> is uploaded to S3
        When the file has been processed
        Then consumption for meter 10000 should be 0

    Scenario: two input files
        Given input file <input-1.txt> is uploaded to S3
        And input file <input-2.txt> is uploaded to S3
        When the file has been processed
        Then consumption for meter 10000 should be 32


