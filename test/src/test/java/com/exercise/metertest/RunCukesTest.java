package com.exercise.metertest;


import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = "com.exercise.metertest.steps",
        features = "src/test/resources/features"
)
public class RunCukesTest { }