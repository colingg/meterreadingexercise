package com.exercise.metertest.helpers;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AwsConfig {
	private String region;
	@JsonProperty("access-key")
	private String accessKey;
	@JsonProperty("secret-key")
	private String secretKey;

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}
}
