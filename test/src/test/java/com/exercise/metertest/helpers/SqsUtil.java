package com.exercise.metertest.helpers;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.Arrays;
import java.util.Map;

public class SqsUtil {

	private final ObjectMapper mapper = new ObjectMapper();

	AwsConfig config = null;

	public boolean isQueueEmpty(String queueUrl) {
		AmazonSQS sqs = getAmazonSQS();
		GetQueueAttributesRequest request = new GetQueueAttributesRequest();
		request.setQueueUrl(queueUrl);
		request.setAttributeNames(Arrays.asList("ApproximateNumberOfMessagesNotVisible", "ApproximateNumberOfMessages"));
		GetQueueAttributesResult queueAttributes = sqs.getQueueAttributes(request);
		Map<String, String> attributes = queueAttributes.getAttributes();
		int count = 0;
		for(String key : attributes.keySet()) {
			count += Integer.parseInt(attributes.get(key));
		}
		return count == 0;
	}

	private AmazonSQS getAmazonSQS() {
		if (config == null) {
			loadConfig();
		}

		AWSCredentials credentials = new BasicAWSCredentials(config.getAccessKey(), config.getSecretKey());
		return AmazonSQSClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials))
				.withRegion(config.getRegion())
				.build();
	}

	private void loadConfig() {
		try (InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("aws.json")) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String content = "", line;
			while((line = reader.readLine()) != null) {
				content += line;
			}
			config = mapper.readValue(content, AwsConfig.class);
		} catch (IOException e) {

		}
	}
}
