package com.exercise.metertest.helpers;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.Iterator;

public class S3Util {

	private final ObjectMapper mapper = new ObjectMapper();

	AwsConfig config = null;

	public void uploadFile(String bucketName, String key, String filePath) {
		AmazonS3 s3 = getAmazonS3();
		s3.putObject(new PutObjectRequest(bucketName, key, new File(filePath)));
	}

	public void clearBucket(String bucketName) {
		AmazonS3 s3 = getAmazonS3();
		ObjectListing objectListing = s3.listObjects(bucketName);
		while (true) {
			Iterator<S3ObjectSummary> objIter = objectListing.getObjectSummaries().iterator();
			while (objIter.hasNext()) {
				s3.deleteObject(bucketName, objIter.next().getKey());
			}
			if (objectListing.isTruncated()) {
				objectListing = s3.listNextBatchOfObjects(objectListing);
			} else {
				break;
			}
		}
	}

	private AmazonS3 getAmazonS3() {
		if (config == null) {
			loadConfig();
		}

		AWSCredentials credentials = new BasicAWSCredentials(config.getAccessKey(), config.getSecretKey());
		return AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials))
				.withRegion(config.getRegion())
				.build();
	}

	private void loadConfig() {
		try (InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("aws.json")) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String content = "", line;
			while((line = reader.readLine()) != null) {
				content += line;
			}
			config = mapper.readValue(content, AwsConfig.class);
		} catch (IOException e) {

		}
	}
}
