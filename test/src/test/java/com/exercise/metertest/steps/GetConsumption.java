package com.exercise.metertest.steps;

import com.exercise.metertest.helpers.S3Util;
import com.exercise.metertest.helpers.SqsUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;
import java.net.URL;

public class GetConsumption {
	private final OkHttpClient client = new OkHttpClient();

	private final ObjectMapper mapper = new ObjectMapper();

	@Given("^database/bucket is cleared$")
	public void clearDB() throws IOException {
		Request request = new Request.Builder()
				.url("http://localhost:8080/meter/clear")
				.delete()
				.build();
		client.newCall(request).execute();
		S3Util s3Util = new S3Util();
		s3Util.clearBucket("meterreadings");
	}

	@Given("^input file <(.*)> is uploaded to S3$")
	public void inputFile(String filename) {
		S3Util s3Util = new S3Util();
		URL url = this.getClass().getClassLoader().getResource("input/" + filename);
		s3Util.uploadFile("meterreadings","input-1", url.getPath() );
	}

	@When("^the file has been processed$")
	public void checkMessageHasBeenProcessed() throws InterruptedException {
		SqsUtil sqsUtil = new SqsUtil();
		while(!sqsUtil.isQueueEmpty("https://sqs.us-west-2.amazonaws.com/896585982979/meterfiles")) {
			Thread.sleep(1000);
		}
	}

	@Then("^consumption for meter (\\d+) should be (\\d+)$")
	public void callConsumptionAPI(Integer id, Integer expected) throws IOException {
		Request request = new Request.Builder()
				.url("http://localhost:8080/meter/" + id + "/consumption")
				.get()
				.build();
		try(Response response = client.newCall(request).execute()) {
			JsonNode content = mapper.readValue(response.body().string(), JsonNode.class);
			Integer consumption = content.get("consumption").asInt();
			assert(consumption).equals(expected);
		}
	}
}
