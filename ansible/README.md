# Ansible Playbook for meter-readings
##### the ansible playbook contains the following:
- scripts to create a s3 bucket with event notification
- scripts to create a sqs queue with appropriate policy
- scripts to create a rds db with appropriate security groups

##### to-do:
- scripts to create redis cache
- security refinements

##### prerequisite:
- ansible
- ansible-vault
- boto

##### run
`ansible-playbook site.yml --ask-vault-pass`

> you need to run `ansible-vault edit dev-variables.yml` to update aws credentials. password is 123456