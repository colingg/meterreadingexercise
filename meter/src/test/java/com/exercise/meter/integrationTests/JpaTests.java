package com.exercise.meter.integrationTests;

import com.exercise.meter.entity.MeterReading;
import com.exercise.meter.repository.MeterReadingRepository;
import com.exercise.meter.service.IncomingFileProcessor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.io.InputStream;

@RunWith(SpringRunner.class)
@DataJpaTest
public class JpaTests {

	@Autowired
	private TestEntityManager testEntityManager;

	@Autowired
	private MeterReadingRepository meterReadingRepository;

	IncomingFileProcessor incomingFileProcessor;


	@Before
	public void contextLoads() throws IOException {
		incomingFileProcessor = new IncomingFileProcessor(meterReadingRepository);
	}

	@After
	public void tearDown() {
		meterReadingRepository.deleteAll();
	}

	@Test
	public void shouldReturnCorrectReadingsFor10000() throws Exception {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("input-1.txt");
		incomingFileProcessor.process(inputStream);

		MeterReading meterReading = meterReadingRepository.findById(10000L).orElse(null);

		assert(meterReading.getLastReading()).equals(10000);
		assert(meterReading.getCurrentReading()).equals(10000);
	}

	@Test
	public void shouldReturnCorrectReadingsFor10000DifferentValues() throws IOException {
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("input-1.txt");
		incomingFileProcessor.process(inputStream);

		InputStream inputStream1 = this.getClass().getClassLoader().getResourceAsStream("input-2.txt");
		incomingFileProcessor.process(inputStream1);

		MeterReading meterReading = meterReadingRepository.findById(10000L).orElse(null);

		assert(meterReading.getLastReading()).equals(10000);
		assert(meterReading.getCurrentReading()).equals(10032);
	}
}
