package com.exercise.meter.sqs;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.DeleteMessageBatchRequestEntry;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.exercise.meter.s3.S3File;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class SQSConsumer extends AbstractSQSConsumer {

    private final ObjectMapper mapper = new ObjectMapper();

    @Value("${sqs.poll-interval:10000}")
    private Integer pollInterval;

    @Value("${sqs.url}")
    private String sqsUrl;

    @Value("${sqs.aws-access-key}")
    private String sqsAwsAccessKey;

    @Value("${sqs.aws-secret-key}")
    private String sqsAwsSecretKey;

    private AmazonSQS sqsClient;

    private ReceiveMessageRequest receiveMessageRequest;

    @Autowired
    private S3File s3File;

    @PostConstruct
    public void readyCheck() {
        completeSubscription();
    }

    @Override
    protected int getPollInterval() {
        return pollInterval;
    }

    @Override
    protected void processMessages() {
        List<Message> messages = sqsClient.receiveMessage(this.receiveMessageRequest).getMessages();

        List<DeleteMessageBatchRequestEntry> deleteBatchRequestEntries = messages.stream().map(message -> {
            try {
                S3EventNotification  s3EventNotification = mapper.readValue(message.getBody(), S3EventNotification.class);
                if (s3File.process(s3EventNotification)) {
                    return new DeleteMessageBatchRequestEntry(message.getMessageId(), message.getReceiptHandle());
                }
            } catch (IOException e) { }
            return null;
        }).filter(Objects::nonNull).collect(Collectors.toList());

        if (deleteBatchRequestEntries.size() > 0) {
            this.sqsClient.deleteMessageBatch(sqsUrl, deleteBatchRequestEntries);
        }
    }

    @Override
    protected void finishConfiguration() {
        this.receiveMessageRequest = new ReceiveMessageRequest(sqsUrl)
                .withMaxNumberOfMessages(1)
                // .withMessageAttributeNames(new String[]{"All"})
                .withWaitTimeSeconds(1);
        sqsClient = AmazonSQSClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(sqsAwsAccessKey, sqsAwsSecretKey)))
                .build();
    }
}
