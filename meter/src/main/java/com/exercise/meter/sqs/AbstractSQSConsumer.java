package com.exercise.meter.sqs;

import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.util.concurrent.ScheduledFuture;

public abstract class AbstractSQSConsumer {

    protected ScheduledFuture<?> existingScheduledRun;
    protected ThreadPoolTaskScheduler scheduler = new ThreadPoolTaskScheduler();

    public AbstractSQSConsumer() {
        this.scheduler.setPoolSize(1);
        this.scheduler.setThreadNamePrefix("SQSListener-");
        this.scheduler.initialize();
    }

    protected abstract int getPollInterval();

    protected abstract void processMessages();

    protected void completeSubscription() {
        if (this.existingScheduledRun != null) {
            this.existingScheduledRun.cancel(false);
        }
        this.finishConfiguration();
        if (this.getPollInterval() > 0) {
            this.existingScheduledRun = this.scheduler.scheduleAtFixedRate(this::processMessages, (long)this.getPollInterval());
        } else {
            this.existingScheduledRun = null;
        }

    }

    protected abstract void finishConfiguration();
}
