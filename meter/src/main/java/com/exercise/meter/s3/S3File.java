package com.exercise.meter.s3;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.exercise.meter.service.IncomingFileProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class S3File {

	@Value("${s3.aws-access-key}")
	private String awsAccessKey;

	@Value("${s3.aws-secret-key}")
	private String awsSecretKey;

	@Value("${s3.region}")
	private String region;

	private AmazonS3 s3;

	@Autowired
	IncomingFileProcessor fileProcessor;

	@PostConstruct
	public void init() {
		if (s3 == null) {
			s3 = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(awsAccessKey, awsSecretKey)))
					.withRegion(region)
					.build();
		}
	}

	public boolean process(S3EventNotification event) throws IOException {
		String objectKey = event.getRecords().get(0).getS3().getObject().getKey();
		String bucketName = event.getRecords().get(0).getS3().getBucket().getName();
		S3ObjectInputStream s3InputStream = s3.getObject(bucketName, objectKey).getObjectContent();

		return fileProcessor.process(s3InputStream);
	}
}
