package com.exercise.meter.service;

import com.exercise.meter.entity.MeterReading;
import com.exercise.meter.repository.MeterReadingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

@Service
public class IncomingFileProcessor {

	private final MeterReadingRepository meterReadingRepository;

	public IncomingFileProcessor(MeterReadingRepository meterReadingRepository) {
		this.meterReadingRepository = meterReadingRepository;
	}

	public boolean process(InputStream inputStream) throws IOException {
		String line;

		BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
		while((line = reader.readLine()) != null) {
			String[] content = line.split(",");

			Long id = Long.parseLong(content[0]);
			Integer reading = Integer.parseInt(content[1]);
			MeterReading meterReading = meterReadingRepository.findById(id).orElse(null);
			if (meterReading != null) {
				updateReading(meterReading, reading);
			} else {
				meterReading = new MeterReading();
				meterReading.setId(id);
				meterReading.setCurrentReading(reading);
				meterReading.setLastReading(reading);
			}
			meterReadingRepository.save(meterReading);
		}

		return true;
	}

	private void updateReading(MeterReading reading, Integer entry) {
	    // ensure there's no duplicates
	    if(reading.getCurrentReading() == reading.getLastReading()) {
	        reading.setLastReading(0);
        }

		List<Integer> readings = Arrays.asList(reading.getCurrentReading(), reading.getLastReading(), entry);
		readings.sort(Integer::compareTo);

		reading.setCurrentReading(readings.get(2));
		reading.setLastReading(readings.get(1));
	}

}
