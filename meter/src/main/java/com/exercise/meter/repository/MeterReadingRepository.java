package com.exercise.meter.repository;

import com.exercise.meter.entity.MeterReading;
import org.springframework.data.repository.CrudRepository;

public interface MeterReadingRepository extends CrudRepository<MeterReading, Long> {
}
