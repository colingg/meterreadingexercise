package com.exercise.meter.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MeterReading {
    @Id
    private Long id;

    private Integer lastReading;

    private Integer currentReading;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getLastReading() {
        return lastReading;
    }

    public void setLastReading(Integer lastReading) {
        this.lastReading = lastReading;
    }

    public Integer getCurrentReading() {
        return currentReading;
    }

    public void setCurrentReading(Integer currentReading) {
        this.currentReading = currentReading;
    }
}
