package com.exercise.meter.controller;

import com.exercise.meter.entity.MeterReading;
import com.exercise.meter.repository.MeterReadingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class ConsumptionController {
    @Autowired
    MeterReadingRepository meterReadingRepository;

    @RequestMapping(path = "/meter/{id}/consumption", method = RequestMethod.GET)
    public ResponseEntity<Map> getConsumption(@PathVariable Long id) {
        MeterReading meterReading = meterReadingRepository.findById(id).orElse(null);

        if (meterReading == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            Map<String, Integer> result = new HashMap<>();
            result.put("consumption", meterReading.getCurrentReading() - meterReading.getLastReading());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
    }

    // for testing purpose only
    @RequestMapping(path = "/meter/clear", method = RequestMethod.DELETE)
    public ResponseEntity clear() {
        meterReadingRepository.deleteAll();
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
