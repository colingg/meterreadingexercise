# Meter Reading Application

This is a straight-forward spring boot application.

#### done:
- when a file is created in s3 bucket, the application is able to pick the event message from sqsm read from the file on s3, then persist the data
- able to call /meter/{id}/consumption to get the consumption
- some tests

#### to-do:
- cache the processed line count in redis so that it can restart from where it fails
- DockerFile
- more tests